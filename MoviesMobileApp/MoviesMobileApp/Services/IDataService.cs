﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoviesMobileApp.Models;

namespace MoviesMobileApp.Services
{
    interface IDataService
    { 
        Task<List<MovieItem>> GetItemsAtAsync(int page);

        int GetCountOfItemsOnPage();
    }
}
