﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoviesMobileApp.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Diagnostics;
using MoviesMobileApp.Settings;
namespace MoviesMobileApp.Services
{
    class DataService : IDataService
    {
        public const string ApiKey = "f14320d4bbf682254a30a9775652c518";
        public const string BaseUrl = "http://api.themoviedb.org/3";
        public const string DiscoverMethod = "/discover/movie"; 
        public const int ItemsOnPage = 20;


        private HttpClient _client;

        public List<MovieItem> Items { get; private set; }

        public DataService()
        {
            _client = new HttpClient();
        }

        public async Task<List<MovieItem>> GetItemsAtAsync(int page)
        {

            Items = new List<MovieItem>();

            try
            {
                var uriString = CreateRequestUri
                (
                    DiscoverMethod,
                    new Dictionary<string, object>()
                    {
                        { "api_key", ApiKey },
                        { "page", page },
                    }
                );

                var response = await _client.GetAsync(uriString);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    var pageData = JsonConvert.DeserializeObject<PageData>(content);
                    Items = pageData.results;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Error:" + e.Message);
                throw;
            }

            return Items;

        }

        public int GetCountOfItemsOnPage()
        {
            return ItemsOnPage;
        }

        private static string CreateRequestUri(string method, Dictionary<string, object> parameters)
        {
            var sb = new StringBuilder();

            sb.Append(BaseUrl);
            sb.Append(method);

            // Add parameters
            bool first = true;
            foreach (var param in parameters)
            {
                var format = first ? "?{0}={1}" : "&{0}={1}";
                sb.AppendFormat(format, Uri.EscapeDataString(param.Key), Uri.EscapeDataString(param.Value.ToString()));
                first = false;
            }

            return sb.ToString();
        }

    }
}
