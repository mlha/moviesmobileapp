﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoviesMobileApp.Models;
using System.Diagnostics;

namespace MoviesMobileApp.Services
{
    class MoviesService : IDataService
    {
        private IDataService _dataService;
        private ICacheService<IMovieItem> _cacheService;
        private int _countOfItemsOnPage;

        public MoviesService(IDataService dataService, ICacheService<IMovieItem> cacheService)
        {
            _dataService = dataService;
            _cacheService = cacheService; 
            _countOfItemsOnPage = dataService.GetCountOfItemsOnPage();
        }

        public async Task<List<MovieItem>> GetItemsAtAsync(int page)
        {
            var movieItems = new List<MovieItem>();

            try
            {
                movieItems = await _dataService.GetItemsAtAsync(page);
               // await _cacheService.AddOrUpdateItems(movieItems);
            }
            catch(Exception e)
            {
                Debug.WriteLine("Exception:" + e.Message);
            }

            return movieItems;//await _cacheService.GetItemsAt(GetCountOfItemsOnPage() * page, GetCountOfItemsOnPage());
        }

        public int GetCountOfItemsOnPage()
        {
            return _countOfItemsOnPage;
        }
    }
}
