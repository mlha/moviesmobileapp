﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoviesMobileApp.Models;
using MoviesMobileApp.Database;

namespace MoviesMobileApp.Services
{
    class CacheService : ICacheService<IMovieItem>
    {
        private MovieDatabase _movieDb;

        public CacheService()
        {
            _movieDb = new MovieDatabase();
        }

        public int Count
        {
            get
            {
                throw new NotImplementedException();
            }
        }


        public Task AddOrUpdateItems(IEnumerable<IMovieItem> Items)
        {
            return Task.Factory.StartNew(() => {
                foreach (var item in Items)
                {
                    _movieDb.InsertOrReplace(item); // insert all?
                }
            });
        }

        public IMovieItem GetItemAt(int index)
        {
            return _movieDb.GetItemAt(index);
        }

        public void InsertOrReplace(IMovieItem movieItem)
        {
            _movieDb.InsertOrReplace(movieItem);
        }

        public Task<List<IMovieItem>> GetItemsAt(int pageOffset, int count)
        {
            //return new 
            throw new NotImplementedException();
        }
    }
}
