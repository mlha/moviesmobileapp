﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoviesMobileApp.Models;

namespace MoviesMobileApp.Services
{
    public interface ICacheService<T>
    {
        Task AddOrUpdateItems(IEnumerable<T> Items);
         
        Task<List<T>> GetItemsAt(int pageOffset, int count);

        T GetItemAt(int index);

        void InsertOrReplace(T item);

        int Count { get; }

    }
}
