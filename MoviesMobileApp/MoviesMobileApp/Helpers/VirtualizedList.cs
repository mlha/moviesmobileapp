﻿using MoviesMobileApp.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesMobileApp.Helpers
{
    public class VirtualizedList<T> : IList<T>
    {
        ICacheService<T> _cacheService;

        public VirtualizedList(ICacheService<T> cacheService)
        {
            _cacheService = cacheService;
        }


        public T this[int index]
        {
            get
            {
                return _cacheService.GetItemAt(index);
            }

            set
            {
                _cacheService.InsertOrReplace(value);
            }
        }

        public int Count
        {
            get
            {
                return _cacheService.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public void Add(T item)
        {
            _cacheService.InsertOrReplace(item);
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(T item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public int IndexOf(T item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, T item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(T item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
