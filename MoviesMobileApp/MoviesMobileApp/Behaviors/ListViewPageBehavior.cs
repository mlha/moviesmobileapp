﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MoviesMobileApp.Behaviors
{
    class ListViewPageBehavior : Behavior<ListView>
    {
        protected override void OnAttachedTo(ListView listView)
        {
            base.OnAttachedTo(listView);

            listView.ItemAppearing += ListView_ItemAppearing;
        }

        protected override void OnDetachingFrom(ListView listView)
        {
            listView.ItemAppearing -= ListView_ItemAppearing;

            base.OnDetachingFrom(listView);
        }

        private void ListView_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
