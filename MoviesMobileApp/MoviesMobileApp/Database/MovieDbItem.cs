﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesMobileApp.Database
{
    class MovieDbItem : Models.IMovieItem
    {
        public int Id { get; set; }
        public int Index { get; set; }

        public string Title { get; set; }
        public string OriginalTitle { get; set; }
        public string PosterPath { get; set; }

        public bool Adult { get; set; }
        public string Overview { get; set; }
        public string ReleaseDate { get; set; }
        public IEnumerable<int> GenreIds { get; set; }
        public string OriginalLanguage { get; set; }
        public string BackdropPath { get; set; }
        public float Popularity { get; set; }
        public int VoteCount { get; set; }
        public float VoteAverage { get; set; }
    }
}
