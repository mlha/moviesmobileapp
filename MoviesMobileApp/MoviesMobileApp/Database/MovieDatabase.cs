﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using System.IO;
using MoviesMobileApp.Models;

namespace MoviesMobileApp.Database
{
    public class MovieDatabase
    {
        SQLiteConnection _db { get; }
        static object _locker = new object();

        public static string RootFolderPath { get; set; } = string.Empty; // is set outside -> platform specific
        public const string DbFileName = "moviedb.db3";

        public MovieDatabase()
        {
            var dbPath = Path.Combine(RootFolderPath, DbFileName);
            _db = new SQLiteConnection(dbPath);

            _db.CreateTable<MovieDbItem>();
        }

        public IMovieItem GetItemAt(int posId)
        {
            lock(_locker)
            {
                return _db.Get<MovieDbItem>(posId);
            }
        }

        public void Update(IMovieItem item)
        {
            lock (_locker)
            {
                _db.Update(item);
            }
        }

        public int Insert(IMovieItem item)
        {
            lock (_locker)
            {
                return _db.Insert(item);
            }
        }

        public int InsertOrReplace(IMovieItem item)
        {
            lock (_locker)
            {
                return _db.InsertOrReplace(item);
            }
        }

        public int DeleteItem(IMovieItem item)
        {
            lock (_locker)
            {
                return _db.Delete(item);
            }
        }

        public IEnumerable<IMovieItem> GetItemsAt(int pageOffset, int count)
        {
            lock (_locker)
            {
                return _db.Query<MovieDbItem>("",new object()).Cast<IMovieItem>();
            }
        }


    }
}
