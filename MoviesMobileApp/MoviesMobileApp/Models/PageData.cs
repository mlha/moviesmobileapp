﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesMobileApp.Models
{
    public class PageData
    {
        public int page { get; set; }
        public List<MovieItem> results { get; set; }
        public int total_results { get; set; }
        public int total_pages { get; set; }
    }
}
