﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesMobileApp.Models
{
    public interface IMovieItem
    {
        string PosterPath { get; set; }
        bool Adult { get; set; }
        string Overview { get; set; }
        string ReleaseDate { get; set; }
        IEnumerable<int> GenreIds { get; set; }
        int Id { get; set; }
        string OriginalTitle { get; set; }
        string OriginalLanguage { get; set; }
        string Title { get; set; }
        string BackdropPath { get; set; }
        float Popularity { get; set; }
        int VoteCount { get; set; }
        float VoteAverage { get; set; }
        int Index { get; set; }
    }
}
