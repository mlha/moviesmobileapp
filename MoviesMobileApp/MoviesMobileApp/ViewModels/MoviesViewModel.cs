﻿using MoviesMobileApp.Helpers;
using MoviesMobileApp.Models;
using MoviesMobileApp.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MoviesMobileApp.ViewModels
{
    public class MoviesViewModel : INotifyPropertyChanged
    {
        private IDataService _movieService;
        private int _loadedPages;
        private bool _isLoading;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<MovieItem> MovieItems { get; private set; }
        public ICommand LoadNextPageCommand { get; private set; }
        public VirtualizedList<MovieItemViewModel> MovieItemsList { get; set; }


        public MoviesViewModel()
        {
           _movieService = new MoviesService(
               new DataService(), 
               new CacheService()
               );

            MovieItems = new ObservableCollection<MovieItem>();

            LoadNextPageCommand = new Command<MovieItem>(async (item) => await ExecuteLoadNextPageCommand(), CanExecuteLoadNextPageCommand);

            ExecuteLoadNextPageCommand();
        }

        public async Task ExecuteLoadNextPageCommand()
        {
            _isLoading = true;
            var newPageItems = await _movieService.GetItemsAtAsync(++_loadedPages);

            foreach (var item in newPageItems)
            {
                item.Number = MovieItems.Count;
                MovieItems.Add(item);
            }

            System.Diagnostics.Debug.WriteLine("cnt:" + MovieItems.Count() + "; pg:" + _loadedPages);
            _isLoading = false;
        }

        public bool CanExecuteLoadNextPageCommand(MovieItem item)
        {
            return !_isLoading && MovieItems.Last() == item;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            try
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }
            catch
            {
            }
        }

    }
}
