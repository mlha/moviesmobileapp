﻿using MoviesMobileApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoviesMobileApp.ViewModels
{
    public class MovieItemViewModel
    {
        public string Title { get; set; }

        public static MovieItemViewModel From(IMovieItem item)
        {
            return new MovieItemViewModel() { Title = item.Title };
        }
    }
}
